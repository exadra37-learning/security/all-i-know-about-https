# All I Know About Https

This repository will containing all my notes about everything I know/learn about HTTPS.

I like to write things down, because this way I learn more deeply the subject and I retain better the information in my 
brain on the long term.


## MENU

* [Https Benefits](All-I-Know-About-Https.md#https-benefits)
* [Http vs Https Speed](All-I-Know-About-Https.md#http-vs-https-speed)
* [Brotli Compression](All-I-Know-About-Https.md#brotli-compression)
* [Tls Handshake](All-I-Know-About-Https.md#tls-handshake)
* [Https and Headers](All-I-Know-About-Https.md#https-and-headers)
	+ [Https Strict Transport Security](All-I-Know-About-Https.md#https-strict-transport-security-hsts)
	+ [Content Security Policy](All-I-Know-About-Https.md#content-security-policy-csp)
	+ [Http Referrer](All-I-Know-About-Https.md#http-referrer)
	+ [Cookies](All-I-Know-About-Https.md#cookies)
* [Content Links](All-I-Know-About-Https.md#content-links)
	+ [Relative Urls](All-I-Know-About-Https.md#relative-urls)
	+ [Relative Paths](All-I-Know-About-Https.md#relative-paths)
* [Https Prevents Man In The Middle Attacks](All-I-Know-About-Https.md#https-prevents-man-in-the-middle-attacks)
* [Https Acronyms](All-I-Know-About-Https.md#https-acronyms)
	+ [CAA - Certificate Authority authorization](All-I-Know-About-Https.md#caa-certificate-authority-authorization)
	+ [CRL - Certificate Revocation List](All-I-Know-About-Https.md#crl-certificate-revocation-list)
	+ [DANE - DNS Based Authentication of Named Entities](All-I-Know-About-Https.md#dane-dns-based-authentication-of-named-entities)
	+ [DNSSEC - Domain Name System Security Extensions](All-I-Know-About-Https.md#dnssec-domain-name-system-security-extensions)
	+ [HPKP - Http Public Key Pinning](All-I-Know-About-Https.md#hpkp-http-public-key-pinning)
	+ [OCSP - Online Certificate Status Protocol](All-I-Know-About-Https.md#ocsp-online-certificate-status-protocol)
	+ [PFS - Perfect Forward Secrecy](All-I-Know-About-Https.md#pfs-perfect-forward-secrecy)
	+ [PKP - Public Key Pinning](All-I-Know-About-Https.md#pkp-public-key-pinning)
	+ [SAN - Subject Alternative Name](All-I-Know-About-Https.md#san-subject-alternative-name)
	+ [SNI - Server Name Indication](All-I-Know-About-Https.md#sni-server-name-indication)
	+ [SSL - Secure Sockets Layer](All-I-Know-About-Https.md#ssl-secure-sockets-layer)
	+ [TLS - Transport Layer Security](All-I-Know-About-Https.md#tls-transport-layer-security)
* [Tools](All-I-Know-About-Https.md#tools)
* [Links](All-I-Know-About-Https.md#links)
	+ [Free Certificates](All-I-Know-About-Https.md#free-certificates)
	+ [Cheat Sheets](All-I-Know-About-Https.md#cheat-sheets)
	+ [Learning Resources](All-I-Know-About-Https.md#learning-resources)
	+ [Blogs](All-I-Know-About-Https.md#blogs)
	+ [Articles](All-I-Know-About-Https.md#articles)

## EXPLICIT VERSIONING

This repository will adhere to this [Explicit Version](https://gitlab.com/exadra37-versioning/explicit-versioning) schema.


## CONTRIBUTING IN ISSUES / MERGE REQUESTS

All contributions are welcome provided that they follow [Contributing Guidelines](CONTRIBUTING.md), where you can find
how to _Create an Issue_ and _Merge Request_.


## AUTHOR

More information about the Author can be found [here](AUTHOR.md).


## CONTRIBUTORS

All contributors can be found [here](CONTRIBUTORS.md).


## LICENSE

This repository uses GPL-3.0 license, that you can find [here](LICENSE).

# All I Know About Https 


## Https Benefits

* Confidentiality - it keeps our info protected, once is always encrypted during all the communication process.
* Integrity - we know the content that is being loaded is the original one, by other words has not been modified.
* Authenticity - SSL certificates guarantees that we are communicating with the original server, not a malicious one.
* SEO - Better SEO ranking on Google for sites using https.
* Brotli Compression - less size in content leads to speed improvements.
* Http 2.0 protocol - allows parallel downloads instead of cascaded ones that translates to great speed improvements 
	and this protocol is only available over https.


## Http vs Https Speed

Using [Http Vs Https Speed](http://www.httpvshttps.com/) website we can see that the http version takes around 
12 seconds to load all images and the https versions only takes around 1 second.

Wow this is amazing, but how can that be possible?

If we open developer tools in the browser and enable in the network tab for the protocol column we will see that in http 
the images are loaded hover http 1.1 protocol while in https they are loaded hover http 2.0 protocol.

On http 1.1 the browser is loading images in cascade, once the number of requests per connection to the server are 
limited, but in http 2.0 protocol this limitation is not present, therefore all images can be requested over same 
connection, by other words they are downloaded in parallel instead of cascade.

But to get this boost in speed the server must support http 2.0 protocol and this protocol is only available in browsers 
Browser support for http 2.0 must be checked [here](http://caniuse.com/#search=http2). 

So if we keep our website in http we will not be able to take advantage of http 2.0 protocol and his incredible boost 
in speed.

Https not only gives us security but also gives us speed ;).


## Brotli Compression

This is a new compression algorithm only available over https, that was [introduced by Google in 2015](https://opensource.googleblog.com/2015/09/introducing-brotli-new-compression.html).

This algorithm improves compression by around 25%.

A more in depth article by Scott Helme can be found [here](https://scotthelme.co.uk/brotli-compression/).


## TLS Handshake

During TLS handshake:

* Client communicates the level of TLS it supports.
* Client communicates the Cipher Suites it supports in the order it prefers to use.
* Server agrees with the client in what level of TLS and Cipher Suites to be used.
* Server provides is public key to the client.
* Client verifies the server public key against is local list of Certificate Authorities(CAs).
* Once verified the public key Client will be confident that is talking with the correct server.
* All previous communication is called the negotiation phase and is not encrypted, but no content is exchanged. This 
	phase is visible to a man in the middle.
* Client exchanges is key with the server, but sends it encrypted with the previous received server public key.
* Server finishes the handshake and the secure communication to exchange content can start.

## Https and Headers

### Https Strict Transport Security (HSTS)

This a header filed we can set to tell the browser to always make the initial and all subsequent requests to our website 
hover https.

`Strict-Transport-Security:max-age=63072000; includeSubdomains; preload`

The max age is a value in seconds.

Include sub domains means that all sub domains of the site will benefit of same security policy.

Preload means that the browser should include it on the preload list. Without adding the site into the 
[HSTS Preload List](https://hstspreload.org/) shipped with the browser, this instruction in the header only takes effect 
in our device browser.

So without including it in the HSTS Preload List this approach still requires a first insecure request hover https to 
get the HSTS header that will enforce the browser to start using always http, therefore to be full secured against Man 
in the Middle Attacks we must submit our site [here](https://hstspreload.org/).

Browser support must be checked [here](http://caniuse.com/#search=hsts).


### Content Security Policy (CSP)

#### Upgrade Insecure Requests

Inside the html head tag we can use a meta tag to upgrade insecure requests.

```html
<html>
	<head>
		<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
	</head>
</html>
```

This is useful to:

* ensure third party content embedded in our page is not loaded over http, like Facebook, Twitter or Disqus comments.
* ensure that in case we forgot in our own code to load something over https it will be forced to use it.

Browser support must be checked [here](http://caniuse.com/#search=upgrade)


#### Block All Mixed Content

Inside the html head tag we can use a meta tag to block all mixed content.

```html
<html>
	<head>
		<meta http-equiv="Content-Security-Policy" content="block-all-mixed-content">
	</head>
</html>
```

Browser support must be checked [here](http://caniuse.com/#search=upgrade)


### Http Referrer

When a user clicks in a http link on a https page the referrer header will not be set in order to protect from 
potentially sending sensitive information in the https referrer link to the http page, because if it was set a man in 
the middle attack could easily obtain that sensitive information.

However the referrer header from an https page to another https page is always set, meaning that sensitive information 
on it will be sent to the destination, therefore exposed to them.

So if the https destination page is not in our control we may not want the referrer header to be set and to control this
we have this options:

* [Content Security Policy Referrer Header](https://scotthelme.co.uk/csp-cheat-sheet/#referrer)
* [The Referrer Policy Header](https://scotthelme.co.uk/a-new-security-header-referrer-policy/)
* [Url Mask Service](https://url.rw/) - This will hide the referrer.
* Shortening URL services should also work to hide the referrer, need to test it.

Support for the referrer policy header can be checked [here](http://caniuse.com/#search=referrer%20policy).


### Cookies

Cookies must always be set with the secure flag enabled to prevent them to be sent over http.


## Content Links

As rule of thumb Relative Urls should be only used for external links and for internal links we should always use 
Relative Paths.

Internal link is any link referencing anything under the same domain we served the content.

External link is any link referencing any link not belonging to the current domain, even a sub domain of the domain 
serving the content.

Given domain `example.com` as the one used to serve the content, than `api.example.com` is considered a external link, 
but `example.com/api` is a internal link.


### Relative Urls

When we use external links in our content we normally use the absolute URL like `http://external-link.com`, but 
this is coupling us to a specific url scheme, the http one an when we implement https we will need to go back to our 
code and find all this urls to fix them.

A best approach is to use a Relative Url where the url scheme is omitted, therefore the browser prepends the url 
scheme used to serve the content any time it needs to use this link.

So instead of using:

* `http://external-link.com` we should use `//external-link.com`

Using `//external-link.com` will be interpreted by the browser as:

* `http://external-link.com` when content is served over http.
* `https://external-link.com` when content is served over https.


### Relative Paths

When serving the content with local assets or linking to internal resources is preferable to use the Relative Path 
instead of the Relative Url.

Every time the browser finds a Relative Path it prepends the url used to serve the content before trying to use it.

So instead of using:

* `//example.com/css/global.css` we should use `css/global.css`
* `//example.com/users/1` we should use `users/1`
* `http://example.com/css/global.css` we should use `css/global.css`
* `http://example.com/users/1` we should use `users/1`

Using `css/global.css` will be interpreted by the browser as:

* `http://example.com/css/global.css` when content is served over http.
* `https://example.com/css/global.css` when content is served over https.


## Https Prevents Man In The Middle Attacks

Man in the middle attack or MITMA is possible when the communication between 2 end points can be intercepted and 
modified, due to the lack of using a secure protocol.

Using https will prevent this attacks to happen, once the attacker can't see the content in traffic in plain text.


### Examples of when it can happen over http

* when a device connects to a wifi router.
* compromised wifi router.
* when the wifi router connects to the ISP using http.
* ISP sniffing due to law enforcement.
* when the ISP connects to the destination several exchange points will be reached and state agents can be sinffing the 
traffic, like governments.


### Examples How they can happen:

* session hijacking due to the use of unprotected cookies, like in Facebook and Twitter cases, by using a tool like 
  [Firesheep](https://techcrunch.com/2010/10/24/firesheep-in-wolves-clothing-app-lets-you-hack-into-twitter-facebook-accounts-easily/).
* loading web forms hover http, even when they post to https, because they may have been modified by a MITMA. For 
  example they may have the post url modified to target a malicious web server, instead of the one serving the website.
* malicious/unauthorized tracking of our activity, by injecting things into the content we send/receive. Some ISPs do this.
* injected malware in a compromised router/isp or any other exchange point. Like the [Tunisia Keystroke logger](https://www.theregister.co.uk/2011/01/25/tunisia_facebook_password_slurping/) inject on Facebook, Gmail and Yahoo login pages loaded hover http.
* dns hijacking by compromising our dns resolvers to serve the site from a server they control, making this way possible 
  for them to change/replace the original content when the site is served hover http. If the site is being served hover 
  https, than to serve their own content they also need the valid SSL certificate for the site.
* routers compromised - like [here](http://www.pcworld.com/article/2926312/large-scale-attack-hijacks-routers-through-users-browsers.html), [here](https://arstechnica.com/security/2014/03/hackers-hijack-300000-plus-wireless-routers-make-malicious-changes/) and [here](https://www.cert.pl/en/news/single/large-scale-dns-redirection-on-home-routers-for-financial-theft/).

### Https helps to mitigate:

* phishing attacks - because when we click in a fake link to a website served hover https, they can't spoof the SSL 
certificate for the original website.
* malicious host files - if the host file in our device is compromised and points a https website to a different 
IP address, the SSL certificate will not match, despite the url in the browser being the correct one.
* Cross Site Request Forgery or CSRF - like on the routers compromised.


## Https Acronyms

Explaining the meaning of some acronyms used when talking about Https implementations. 

### CAA - Certificate Authority Authorization

At the DNS level we can specify what Certificate Authorities we allow to issue certificates for the domain.

This also protects us from Certification Authorities that have been compromised, like the already mentioned 
[DigiNotar Iran](https://www.eff.org/deeplinks/2011/09/post-mortem-iranian-diginotar-attack) case.


### CRL - Certificate Revocation List

This is a list that each Certification Authority needs to maintain to list all is revoked certificates, so that all 
clients using it can know what certificates they can't trust any more.

So this is a list that will be shipped with the browser or with the Operating System, therefore making the revocation 
process slow and dependent on our system updates.


### DANE - DNS Based Authentication of Named Entities

Used to specify at DNS level the certificate keys being used. DNSSEC must be enabled to use DANE.

This helps to protect the compromise of a Certificate Authority, like the famous [DigiNotar Iran](https://www.eff.org/deeplinks/2011/09/post-mortem-iranian-diginotar-attack) case.

A brief explanation how they Certificate Authorities can be attacked can be found [here](https://www.vidder.com/compromised-ca-certificate-attacks/).


### DNSSEC - Domain Name System Security Extensions

Prevents the forgery of DNS records for the domain, guaranteeing this way the integrity of the Domain.

The forgery of DNS records allows an attacker to route the traffic to their own servers to what so ever they want with it.


### HPKP - Http Public Key Pinning

Defines the certificate public keys that are admissible by the client for each domain.

In is attributes we can:

* list all the public keys for allowed the certificate.
* specify the max age.
* tell if it applies to sub domains.
* provide the url to report violations for attempts on use of invalid public keys.

This is implemented in the Response with the `public-key-pins` header.

A draw back of this is that we need to have a first trusted request, so that subsequent calls to the domain can be 
protected from man in the middle attacks.


### OCSP - Online Certificate Status Protocol

An alternative to CRL where the Certificate Authorities implement the [OCSP protocol](https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol) to handle almost
in real time the certificates revocation.

OCSP can cause some privacy issues, therefore [OCSP Stapling](https://en.wikipedia.org/wiki/OCSP_stapling) is preferred.

In a OCSP implementation the website is responsible to validate the certificate by calling the OCSP server of the 
Certification Authority and return the result in the TLS hand shake between server and client.


### PFS - Perfect Forward Secrecy

In case a private key for the certificate is compromise it protects any past sessions encrypted with that private key 
to become also compromised.


### PKP - Public Key Pinning

The client will only accept certificates from a predefined list with the public keys of the certificates it can trust on.

This by itself does not guarantee to the client that a certificate in the predefined list is the correct one for the 
domain... Remember the [DigiNotar Iran](https://www.eff.org/deeplinks/2011/09/post-mortem-iranian-diginotar-attack) case.


### SAN - Subject Alternative Name

Makes possible to use multiple domain names in the same certificate.


### SNI - Server Name Indication

Enables multiple certificates in the same IP address.


### SSL - Secure Sockets Layer

SSL was commonly used until 2014 Poodle attack to implement HTTPS but is now suppressed by TLS.


### TLS - Transport Layer Security

Nowadays TLS is the standard to implement HTTPS, once is more secure and fast and with it the Poodle attack would have 
not succeeded.


## Tools 

* [Automated Installation of Certificates](https://certbot.eff.org/) - Automates the process of acquire/renew/install Let's Encrypt certificates.
* [SSL Server Test](https://www.ssllabs.com/ssltest/index.html) - The famous Qualys SSL Labs tester.
* [Security Headers Checker](https://securityheaders.io/)
* [Real Time Security Reporting](https://report-uri.io/)
* [Fiddler](http://www.telerik.com/fiddler) - Proxy to debug Http and Https.
* [BadSSL](https://badssl.com/) - site to show all that can go wrong with an Https implementation.
* [HSTS Preload](https://hstspreload.org/) - verifies if a site is in the browsers https preload list, if they are eligible to be there or 
  to submit a site to this preload list. 
* [Chromium Preload List](https://cs.chromium.org/chromium/src/net/http/transport_security_state_static.json) - List of all preloaded sites.
* [Browser Compatibility Checker for HSTS](http://caniuse.com/#search=hsts).
* [Http Vs Https Speed](http://www.httpvshttps.com/) - Compare speed of loading this website hover http vs https connection. 


## Links

### Free Certificates

* [Lets's Encrypt](https://letsencrypt.org/) - Let's Encrypt gives us certificates for free with a 3 months expiring period.


### Cheat Sheets

* [Scott Helme Cheat Sheet](https://scotthelme.co.uk/https-cheat-sheet/)


### Learning Resources

* [Troy Hunt - What Every Developer Should Know About Https](https://app.pluralsight.com/library/courses/https-every-developer-must-know/table-of-contents)
* [SSL Labs Wiki](https://github.com/ssllabs/research/wiki)
* [Open SSL CookBook](https://www.feistyduck.com/library/openssl-cookbook/)
* [Bullet Proof SSL and TLS](https://www.feistyduck.com/books/bulletproof-ssl-and-tls/reviewerKit.html)


### Blogs

* [troyhunt.com](https://troyhunt.com)
* [scotthelme.co.uk](https://scotthelme.co.uk)
* [blog.ivanristic.com](https://blog.ivanristic.com/)


### Articles

* [Is TLS Fast Yet](https://istlsfastyet.com/) - website about speed impact when using secure communications.
* [DigiNotar CA Compromised](https://www.eff.org/deeplinks/2011/09/post-mortem-iranian-diginotar-attack) - The history of how Iran compromised this CA to spy on is citizens email accounts.
* [Certificate Authorities Attacks](https://www.vidder.com/compromised-ca-certificate-attacks/) - A brief explanation how they can get compromised.

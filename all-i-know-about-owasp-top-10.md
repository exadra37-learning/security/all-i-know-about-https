# All I Know About OWASP Top 10

[OWASP - Open Web Application Security Project](https://www.owasp.org/index.php/Main_Page) is non profit and technology
agnostic organization that relies in the community to exist.

Time to time they release the OWASP top 10 that can be found [here](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project).

## OWASP Top 10

### 1. [Injection](https://www.owasp.org/index.php/SQL_Injection)

The most common injection risk is the SQL injection, once is the most common type of database used.

Some types of SQL Injection are:

* error based injection
* union based injection
* blind injection

#### Sql Injection in the Url

Given a url `example.com/users?id=1` that translates to this sql query `select * from users where id = 1` the attacker
can mount a sql injection to exploit the fact that the value `1` in the string is not treated as untrusted by the 
developers, therefore used directly in the sql query.

The attack can look like `example.com/users?id=1 or 1=1` that will translate to the sql query `select * from users where id=1 or 1=1`,
given this way all the users info in the database to the attacker, because `1=1` will evaluates always to `true`.

This attack is only possible because the developers don't treat the input values as untrusted and use them directly in 
the query without any validation.

The sql injection can also be placed in the headers, post fields or in the payload.


#### Possible Defences

* Whitelist - it will define what input can be trusted.
* Validation - check if it adhere to the expected patterns.
* Parametrise SQL statements 
	+ Getting the input data must be separated from building the query.
	+ Query must be build using parameters.
	+ Type cast each parameter in the query.
* Database Users and Permissions
	+ Always use a user that follows the principle of least privilege.
		. Separate database users for admin tasks from the ones used for performing public tasks.
		. All resources that are public facing should use a database user with limited privileges, that only have the 
		  necessary permissions to accomplish their tasks.
	+ Segment access to database by resource.
		. The login resource should only have access to the database tables strictly necessary to log on the user.
		. A resource to list products in a database should not have access to unrelated tables, like the users table.

#### Real Cases

The Sony Pictures [LuzSec's Attack](http://www.bbc.co.uk/news/business-13636704) is very famous example of this type of
attack.


### 2. [Broken Authentication and Session Management](https://www.owasp.org/index.php/Broken_Authentication_and_Session_Management)

When a User is logged on in a system, each time he request something from it some authentication info must be provided
to the system to identify the user and confirm is already logged.

If this info gets hijacked by an attacker, than he can impersonated the request and the system will assume that is the 
legit user doing it.


#### Authentication Cookie 

The authentication cookie can be hijacked via:

* Exploiting an XSS vulnerability.
* Sniffing over an insecure connection.
* Directly from the victim device via malware or direct access.


#### Session Id or Token

The most common identifiers are the Session Id or Token, but they can be found in many other flavours, like api key, 
client key, etc. but we will refer to it simply as identifier.

The identifier may be in the cookies, url, headers or in the payload.

Due to the design of the system sometimes all it is needed to authenticate the user is this identifier.

When the identifier is in the Url, the risk for being hijacked is bigger as it can be done via:

* Copy paste the url containing it from:
	+ A email.
	+ A social media post.
	+ On forums when people try to get help for an issue.
	+ In code repositories, like Github or Gitlab.
	+ Sniffing over an insecure connection.
* Retrieving it from logs:
	+ on the compromised server.
	+ in the error reporting system, not properly secured, like commonly seen in Elastic Search.


#### Login Attacks

In order to get access to privileged areas in a system, public or administrative ones, normally the user needs to 
login/register and often the attacker try to exploit this process to gain access to the system as an admin or regular user.

Some examples how can be done:

* Brute force the login:
	+ System does not limit number of attempts of failed logins. An dictionary attack can be easily performed.
	+ Enumeration:
		. system tells if the password is incorrect or if the email is wrong or not exists, instead of simply say that 
		  provided credentials are incorrect.
  		. an attacker can easily check if a user name is already registered before it try to brute force login on it.
* Exploit password reset mechanism:
	+ when the recover process relies solely on security questions, because often this questions are silly and easily 
	  discoverable or public accessible.
	+ when it resets the password to a default one, used across all resets.
	+ password resets that email a plain text password.
* Weak credentials
	+ Allowing dictionary passwords.
	+ Not allowing special characters in the password. Very common in UK.
	+ Not enforcing capital letters and special characters in the password.
	+ Small length off password - nowadays 12 alphanumeric characters should be a good starting point in my opinion.


#### Possible Defences

* Cookies Protection
	+ Set the HttpOnly flag to prevent cross-site scripting use of cookies.
	+ Set the Secure flag to prevent them to be sent over http, therefore susceptible to be sniffed by an attacker.
* Reduce the Risk
	+ Expire sessions in short periods of time. This is a trade off between security and usability, therefore a balance 
	  must be found depending on the risks involved.
	+ Ask for re-authentication on important actions performed by the user, even if the user has logged in 1 second ago.
	+ Always ask for the old password to allow a password reset and perform some challenges:
		. send email verification to confirm/trigger password reset.
		. mobile challenge via app or sms.
* Logins
	+ rate limit failed attempts and apply temporary lockouts.
	+ apply permanent lockout after some temporary lockouts.
	+ notify promptly the user of failed attempts to login.
* Passwords 
	+ Allow copy paste on login forms so that password managers can be used.
	+ Enforce strong passwords:
		. with length greater than 12 characters.
		. with capital letters.
		. with special characters.
	+ Blacklist dictionary passwords.
	+ Don't allow passwords containing:
		. email
		. username
		. any other personal info of the user
	+ Encourage use of 2FA mechanisms. They are not bullet proof but they are one more layer of defence.


#### Real Cases

* [Hackers can Steal your Apple ID in 10s](http://shootitlive.com/2012/10/hackers-can-steal-your-appleid-in-10-s/)
* [Apple IOS Flaw Enables Attacks via Hotspot](https://www.infosecurity-magazine.com/news/apple-ios-flaw-enables-attacks-via/)


### 3. [Cross-Site Scripting (XSS)](https://www.owasp.org/index.php/Cross-site_Scripting_(XSS))

It happens when a user clicks in a url with a XSS payload.

The XSS attack can have 2 types:

* XSS reflected
* XSS persisted

Given a user clicking in a url `example.com/search?query=scotland` that it founds for example in a social media post,
the user will land in a search result page of site `example.com` and this is not armful at all.

Now if the url have the XSS payload `example.com/search?query=scotland<script>alert(document.cookies)</script>` when 
user clicks on it will land on same result page, but this time a popup box will show all stored cookies for the site.

This is possible because normally the search query shows up in the search result page, therefore if the query string 
`scotland` was not treated as untrusted it will be part of the html as a valid script tag, allowing the JavaScript to be 
executed on page load. This is a XSS Reflected attack.

Usually all searches are logged into a database, therefore the XSS payload is persisted. Now all that it takes for a XSS
Persisted attack to happen is for some user to check the search queries in the admin interface, that display them as 
trusted data, therefore using them in the html without encode it with html entities or stripping tags.

This shows how dangerous a XSS attack can be, depending on the attacker intentions, because of instead the pop-up alert 
to show the cookies a call to a site controlled by the hacker can be made by the XSS payload to hijack all the cookies.

If that cookies contain authentication info than the attacker can visit the site from is computer has if it was the 
legit user.


#### Possible Defences

* Whitelist - it will define what input can be trusted.
* Validation - check if it adhere to the expected patterns.
* Encode
	+ Output should be always encoded, no matter the source, even from our database.
	+ Encode must be done depending on the context it will be used. Different encode will be used depending if used in 
	  Html, Javascript or Css.

#### Real Cases

* The very popular [Samy's MySpace XSS Hack](https://en.wikipedia.org/wiki/Samy_Kamkar) that changed the security industry forever.
* [Samy's Site](https://samy.pl/popular/).
* Check this [Security Stack Excahange](https://security.stackexchange.com/questions/95256/famous-xss-attacks-over-javascript) question for more real cases.


### 4. [Insecure Direct Object References](https://www.owasp.org/index.php/Top_10_2013-A4-Insecure_Direct_Object_References)

This happens by manipulating the url to access data that should not be accessed by the user.

Given a user seeing is account balance with the url `mybank.com/balance?account_id=123456`, by changing the account id 
directly in the url to another value, `mybank.com/balance?account_id=987654`, it should not be possible for him to see 
the balance of the other user.

#### Possible Defences

* Access controls
	+ Be explicit about who can access the resources.
	+ Have rules in place to be checked each time the resource needs to be used.
* Indirect Reference Maps
	+ map the real reference in database to a temporary one, that will be exposed externally and stored in the user session.
	+ this makes sense to use when exposing sensitive data, but is not necessary in a e-commerce site for product or 
	  category ids direct references protection.
* Avoid Predictable Keys
	+ Integers are easily enumerable and easy to brute force attack.
	+ Natural keys, like user-names are also easily discoverable.
	+ Better to use unique identifiers cryptography generated.

Access controls must be always used as the primary line of defence.

Indirect Reference Maps may be used only to not expose direct references to sensitive data. 

Avoiding to use predictable keys is a last line of defence and his use increases security but adds complexity in how 
data will be stored in database, therefore trade off's must be considered.


#### Real Cases

* [Citigroup Url Hack](https://www.theregister.co.uk/2011/06/14/citigroup_website_hack_simple/) to manipulate user account ids.

